#ifndef __LOGGER_H___
#define __LOGGER_H__

#include<zmq.h>
#include<string>
#define INACTIVE 0
#define ACTIVE 1

namespace zmq_logger
{
class Log_Topic
{
	private:
	//Topic Name: Can be enhanced by enumerating the Topics list
	//For now only contains the name of the topic
	char *Log_Topic_name;
	//status wheather the topic has already been created.
	// default : not created
	bool Log_Topic_Status;

	Log_Topic()
	{
	}
	Log_Topic(char *);
	~Log_Topic()
	{
	}
	public:
	bool get_topic_status();
	bool set_topic_status();
};

class ZeroMQ
{
	private:
	void * ctx;
	void * socket;
	int zmq_socket_type;
	char * zmq_network_interface;
	char * zmq_protocol;
	char * zmq_IP_addr;
	char * zmq_port_num;
	int zmq_conn_status;
	void zmq_create_socket(void *, int);
	void zmq_create_conn();
	int zmq_get_socket_type();
	char * zmq_get_network_interface();
	char * zmq_get_protocol();
	char * zmq_get_port();
	char * zmq_get_IP();
	//int zmq_bind_port(void *, char *);
	int zmq_start_conn(void *, char *);
	int zmq_shutdown();

	public:
	ZeroMQ();	
	~ZeroMQ()
	{
	}

	int zmq_get_conn_status();
	void zmq_set_conn_status(int );
	int zmq_write_log(char *);
	void log_error(const char * );
};

class Connection :
		protected ZeroMQ
{
	int log_level;
	class Log_Topic *topic;
	int log_get_topic_status(class Log_Topic *);
	int log_set_topic_status(int);
	class Log_Topic* log_create_topic(char *);
	char * log_format_msg(class Log_Topic, ...);
	int log_write_to_zmq(char *);

	public:
	Connection();
	~Connection()
	{
	}

	void log_msg(Log_Topic *, char *, ...);
};
};

#endif
