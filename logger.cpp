#include<iostream>
#include<string.h>
#include<string>
#include"logger.h"
#include<stdarg.h>
#include<mutex>
#include<zmq.h>
#include<unistd.h>
using namespace std;
using namespace zmq_logger;
std::mutex mtx;

void
Connection::
log_msg(class Log_Topic *topic, char * msg, ...)
{
	const char * str;
	int msg_write_flag;
        va_list Args;
        va_start(Args,msg);
        
        while ((str = va_arg(Args, const char *)) != NULL)
                msg = strcat(msg, (char *)str);

        va_end(Args);

	
//	char *formatted_msg = log_format_msg(topic,Args);
	msg_write_flag = zmq_write_log(msg);
	if (msg_write_flag == 0)
		return;
	else
	{
		const char *error_string = "Unable to write Log_msg to ZMQ";
		log_error(error_string);
		return;
	}
}

Connection::
Connection()
{
cout << " Connection constructor" << endl;
}
#if 0
void
Connection::
log_msg(class Log_Topic *topic, char * msg)
{
        int msg_write_flag;

        msg_write_flag = zmq_write_log(msg);
        if (msg_write_flag == 0)
                return;
        else
        {
                const char *error_string = "Unable to write Log_msg to ZMQ";
                log_error(error_string);
                return;
        }
}
#endif

/*
char *
Connection::
log_format_msg(Log_Topic *topic, ...)
{
}
*/

ZeroMQ::
ZeroMQ()
{
	ctx = NULL;
	socket = NULL;
	zmq_socket_type = 0;
	cout<< "ZeroMQ constructor" << endl;
	this->zmq_conn_status = -1;
	this->zmq_create_conn();
}

void
ZeroMQ::
zmq_create_conn()
{
	if(zmq_get_conn_status() < 0)
	{
		this->ctx = zmq_ctx_new();
		if(ctx == NULL)
		{
			this->zmq_set_conn_status(-1);
			this->log_error("Error creating ZMQ context");
			return;
		}
		// Get the port details from the configuration file
		// Get the interface name and protocol name. 
		//form a string as tcp://eth0:5555
		this->zmq_IP_addr = zmq_get_IP();
		// Create socket
		socket = zmq_socket(ctx, zmq_socket_type);
		if(socket == NULL)
		{
                        this->zmq_set_conn_status(-1);
                        this->log_error("Error creating ZMQ Socket");
                        return;
		}
		this->log_error("Socket Created successfully");
		// Establish Connection using IP string 
		zmq_conn_status = zmq_connect(socket, "tcp://192.168.56.102:5555");
                if(zmq_conn_status < 0)
                {
                        this->zmq_set_conn_status(-1);
                        this->log_error("Connection failed");
                        return;
                }
		cout << " connected" << this->zmq_conn_status << endl;

                char *buff;
                char tmp[] = "first attempt";
                buff = (char *)malloc(sizeof tmp);
                memcpy(buff, &tmp, sizeof(tmp));
                int count = zmq_send(socket,buff,strlen(buff),ZMQ_SNDMORE);
                cout << "written count" << count << endl;
                count = zmq_send(socket,buff,strlen(buff),0);
                count = zmq_recv(socket,buff,strlen(buff),ZMQ_DONTWAIT);
                cout << "received count " << count << endl;

	}
	this->log_error("Connection Active");
	return;
}

char *
ZeroMQ::
zmq_get_network_interface()
{
	// Read from config file or use default
	const char * iface = "eth0:";
	return (char *)iface;
}

char *
ZeroMQ::
zmq_get_protocol()
{
        // Read from config file or use default
	const char * prot = "tcp://";
	return (char *)prot;
}



char *
ZeroMQ::
zmq_get_port()
{
        // Read from config file or use default
	const char * port = "tcp://eth0:5555";
	return (char *)port;
}

char * 
ZeroMQ::
zmq_get_IP()
{
        // Read from config file or use default
	const char * ip_addr = "tcp://192.168.56.102:5555";
	return (char *)ip_addr;
}

int ZeroMQ::
zmq_get_socket_type()
{
	return 3;
}

//create a socket. zmq socket is of type void *
void
ZeroMQ::
zmq_create_socket(void *ctx, int socket_type)
{
	//void *sock = zmq_socket(ctx, socket_type);
	this->socket = zmq_socket(ctx, ZMQ_REQ);
	return;
}

// bind socket to a port, takes protocol and interface with port number
// as port_string. return 0 on success -1 on failure
/*
int
ZeroMQ::
zmq_bind_port(void * socket, char * port_string)
{
	//int rc = zmq_bind(socket, port_string);
	int rc = zmq_bind(socket,"tcp://eth0:5000");
	return rc;
}
*/
// bind socket to a port, takes protocol, with IP address and port number
// as port_string. return 0 on success -1 on failure
int 
ZeroMQ::
zmq_start_conn(void *socket, char * IP_string)
{
	int rc = zmq_connect(socket,"tcp://192.168.56.102:5000");
	return rc;
}

int 
ZeroMQ::
zmq_get_conn_status()
{
	return this->zmq_conn_status;
}

void 
ZeroMQ::
zmq_set_conn_status( int flag)
{
	this->zmq_conn_status = flag;
}

int 
ZeroMQ::
zmq_write_log( char * msg)
{
	int count = zmq_send(this->socket, msg, strlen(msg), ZMQ_SNDMORE);
	return count;
}

void 
ZeroMQ::
log_error(const char * str)
{
	cout << str << endl;
}
